export  default function slidersInit(){
	
	(function($){
	
	
    //slider on front_page
    $(".owl-1").owlCarousel({
        // responsive: {
        //     0: {
        //         items: 3,
        //         stagePadding: 0
        //     },
        //     980: {
        //         items: 2,
        //         stagePadding: 20
        //     },
        //     // 1100: {
        //     //     items: 2,
        //     //     stagePadding: 80
        //     // },
        //     1360: {
        //         items: 2,
        //         stagePadding: 100
        //     },
        //     1600: {
        //         items: 3,
        //         stagePadding: 20
        //     }
        // },
        items: 1,
        loop: true,
        autoplay: 2000,
        stopOnHover: true,
        smartSpeed: 1000,
        slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        dots: true,
        nav: true,
        navText:[`<svg xmlns="http://www.w3.org/2000/svg" width="22.026" height="34.356" viewBox="0 0 22.026 34.356">
        <path id="Path_280" data-name="Path 280" d="M28.5,34.447l-12.3-12.33L28.5,9.788,24.708,6,8.59,22.117,24.708,38.234Z" transform="translate(-7.529 -4.939)" fill="#3f5ba5" stroke="#fff" stroke-width="1.5"/>
      </svg>`, `<svg xmlns="http://www.w3.org/2000/svg" width="22.025" height="34.356" viewBox="0 0 22.025 34.356">
      <path id="Path_3" data-name="Path 3" d="M8.59,34.447l12.3-12.33L8.59,9.788,12.378,6,28.495,22.117,12.378,38.234Z" transform="translate(-7.53 -4.939)" fill="#3f5ba5" stroke="#fff" stroke-width="1.5"/>
    </svg>`],
        // autoHeight: true
    });






    //slider partnerzy

    $(".owl-2").owlCarousel({
        responsive: {
            0: {
                items: 3,
                margin: 40,
                nav: false
               
            },
            640: {
                items: 4,
                margin: 40,
                nav: false
            },
            830: {
                items: 4,
                margin: 60,
                nav: true,
            },
            1200: {
                items: 5,
                margin: 50
            }
        },
        loop: true,

        autoplay: 2000,
        stopOnHover: true,
        smartSpeed: 850,
        slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        nav: true,
        // autoplayTimeout:1000,
        dots: false,
        autoHeight: true,
        	navText:[`<svg xmlns="http://www.w3.org/2000/svg" width="22.026" height="34.356" viewBox="0 0 22.026 34.356">
            <path id="Path_280" data-name="Path 280" d="M28.5,34.447l-12.3-12.33L28.5,9.788,24.708,6,8.59,22.117,24.708,38.234Z" transform="translate(-7.529 -4.939)" fill="#3f5ba5" stroke="#fff" stroke-width="1.5"/>
          </svg>`, `<svg xmlns="http://www.w3.org/2000/svg" width="22.025" height="34.356" viewBox="0 0 22.025 34.356">
          <path id="Path_3" data-name="Path 3" d="M8.59,34.447l12.3-12.33L8.59,9.788,12.378,6,28.495,22.117,12.378,38.234Z" transform="translate(-7.53 -4.939)" fill="#3f5ba5" stroke="#fff" stroke-width="1.5"/>
        </svg>`],
    });
		
	})(jQuery);
	
}
 